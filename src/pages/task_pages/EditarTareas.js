import React, { useState, useEffect } from 'react';
import ContentHeader from "../../components/ContentHeader";
import Navbar from "../../components/Navbar";
import swal from 'sweetalert';
import APIInvoke from '../../utils/APIInvoke';
import { useNavigate, useParams } from 'react-router-dom';

const EditarTareas = () => {

    const navigate = useNavigate();

    const { idtarea } = useParams();

    const obtenerTarea = async () => {
        const response = await APIInvoke.invokeGET(`/api/tareas/${idtarea}`);
        setTareas(response);
    }
    
    useEffect(() => {
        obtenerTarea();
        document.getElementById("titulo").focus();
    }, [])

    const [tareas, setTareas] = useState({
        titulo: '',
        descripcion: '',
        estado: ''
    });

    const { titulo, descripcion, estado } = tareas;

    

    const onChange = (e) => {
        setTareas({
            ...tareas,
            [e.target.name]: e.target.value
        })
    }

    const editarTarea = async () => {
        const data = {
            titulo: tareas.titulo,
            descripcion: tareas.descripcion,
            estado: tareas.estado
        }

        const response = await APIInvoke.invokePUT(`/api/tareas/${idtarea}`, data);

        if (response.acknowledged === false) {
            swal("Tarea rechazada", "No se ha podido actulizar la tarea");
        } else {
            navigate("/tareas");
            swal("Tarea actualizada", "Se ha actualizada satisfactoriamente");
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarTarea();
    }

    return (
        <div className="content">
            <Navbar></Navbar>

            <div className="container">

                <ContentHeader
                    Titulo={"Edición de tarea"}
                    breadCrumb1={"Lista de Tareas"}
                    breadCrumb2={"Edición de tarea"}
                    ruta1={"/tareas"}
                />

                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                            <h1 class="card-title"><i class="fa fa-tasks mr-3" aria-hidden="true" />Editar tareas</h1>
                                <div className="card-tools">
                                    <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i className="fas fa-minus" />
                                    </button>
                                </div>
                            </div>
                            <div className="card-body p-0">

                                <form onSubmit={onSubmit}>
                                    <div className="card-body">
                                        <div className="form-group">
                                            <label htmlFor="titulo">Titulo</label>
                                            <input type="text"
                                                className="form-control"
                                                id="titulo"
                                                name="titulo"
                                                placeholder="Titulo Tarea"
                                                value={titulo}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="descripcion">Descripción</label>
                                            <input type="text"
                                                className="form-control"
                                                id="descripcion"
                                                name="descripcion"
                                                placeholder="Descripcion Tarea"
                                                value={descripcion}
                                                onChange={onChange}
                                                required
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="estado">Estado</label>
                                            <input type="text"
                                                className="form-control"
                                                list="estado"
                                                name="estado"
                                                placeholder="En espera - En proceso - Finalizada"
                                                value={estado}
                                                onChange={onChange}
                                                required
                                            />
                                            <datalist id="estado">
                                                <option value="En espera"></option>
                                                <option value="En proceso"></option>
                                                <option value="Finalizada"></option>
                                            </datalist>
                                        </div>
                                        <button type="submit" className="btn btn-primary">Editar</button>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default EditarTareas;