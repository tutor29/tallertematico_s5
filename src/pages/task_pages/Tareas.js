import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import ContentHeader from "../../components/ContentHeader";
import Navbar from "../../components/Navbar";
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert';

const Tareas = () => {

    const [tareas, setTareas] = useState([]);

    const cargarTareas = async () => {
        const response = await APIInvoke.invokeGET(`/api/tareas`);
        setTareas(response);
    }

    useEffect(() => {
        cargarTareas();
    }, [])

    const eliminarTarea = async (e, idTarea) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/tareas/${idTarea}`);

        if (response.acknowledged === false) {
            swal("Tarea rechazada", "No se ha podido eliminar la tarea");
        } else {
            swal("Tarea eliminada", "Se ha eliminado la tarea satisfactoriamente");
            cargarTareas();
        }
    }

    return (
        <div className="content">
            <Navbar></Navbar>

            <div className="container">

                <ContentHeader
                    Titulo={"Lista de Tareas"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Tareas"}
                    ruta1={"/inicio"}
                />

                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                                <h1 class="card-title"><i class="fa fa-tasks mr-3" aria-hidden="true" />Listado de Tareas</h1>
                                <div className="card-tools">
                                    <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i className="fas fa-minus" />
                                    </button>
                                </div>
                            </div>
                            <div className="card-body p-10">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style={{ width: '70%' }}>Titulo</th>
                                            <th style={{ width: '15%' }}>Estado</th>
                                            <th style={{ width: '15%' }}>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            tareas.map(
                                                item =>
                                                    <tr key={item._id}>
                                                        <td>{item.titulo}</td>
                                                        <td>{item.estado}</td>
                                                        <td>
                                                            <Link to={`/tareas/${item._id}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;
                                                            <button onClick={(e) => eliminarTarea(e, item._id)} className="btn btn-sm btn-danger">Eliminar</button>
                                                        </td>
                                                    </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                                <h1 className="card-title"><Link to={"/tareas/crear"} type="button" className="btn btn-block btn-primary btn-sm mt-3 p-2 t-z-20">Crear Tarea</Link></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Tareas;