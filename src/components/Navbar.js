import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a href="/inicio"><img src="https://misiontic2022.mintic.gov.co/mtv2/assets/assets/images/logo-mision.png" width="70" height="40"/></a>
                <Link className="nav-link" to={"/inicio"} role="button"> INICIO </Link>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-link" to={"/tareas"} role="button"> Lista de Tareas </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Navbar;