import React, {Fragment} from "react";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Inicio from "./pages/Inicio";
import CrearTareas from "./pages/task_pages/CrearTareas";
import EditarTareas from "./pages/task_pages/EditarTareas";
import Tareas from "./pages/task_pages/Tareas";


function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Tareas/>}/>
          <Route path="/inicio" exact element={<Inicio/>}/>
          <Route path="/tareas" exact element={<Tareas/>}/>
          <Route path="/tareas/crear" exact element={<CrearTareas/>}/>
          <Route path="/tareas/:idtarea" exact element={<EditarTareas/>}/>
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
